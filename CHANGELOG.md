# CHANGELOG

<!--- next entry here -->

## 0.1.0
2019-09-18

### Features

- **backend-244:** Feed proxy (23407147c1f0d3715fde98d03dcd7d3d5022bf71)
- **backend-244:** Fix pipeline (a8d8533edb57925fd638da29a95919cd50599117)

### Fixes

- **backend-244:** Fix app boot (e1121e2e2f4d4b7a06b340c5e4f56fb1b0f05116)
- **backend-244:** Added firebase-admin.json to artefacts (846ea3e7da4ad7ce027e08233a8ed15451b091e6)
- **backend-244:** Fix boot (14bcd390d23a7a84a589e27dbbcb74bb8142a730)
- **backend-244:** Converted project to JS 🤮 (4cec7b93e335f0790a58184492587ea0d553949e)
- **backend-244:** Fix pipeline (8f2fb401ed1e5c6f99392fcab938aa09d62e2e49)