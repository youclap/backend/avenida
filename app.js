const express = require('express');
const logger = require('morgan');
const admin = require('firebase-admin');


const app = express();

let serviceAccount = require('./firebase-admin.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const feedRouter = require('./routes/feed');
const healthcheckRouter = require('./routes/healthcheck');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/feed', feedRouter);
app.use('/healthcheck', healthcheckRouter);

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
