var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {

  console.log("🚑 Health check")

  res.status(204);
  res.send();
});

module.exports = router;
