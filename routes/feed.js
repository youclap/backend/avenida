const express = require('express');
const admin = require("firebase-admin");
const router = express.Router();

const firestore = admin.firestore();

router.get('/', async function(req, res, next) {

  console.log("🍆 headers", req.headers)

  const profileID = req.headers["x-profile-id"] 
  const authUserID = req.headers["x-auth-user"]
  const limit = parseInt(req.query["limit"]) || 10
  const nextToken = req.query["nextToken"]
  
  console.log("ℹ️ authUserID", authUserID)
  console.log("️️️ℹ️ profileID", profileID)
  console.log("ℹ️ limit", limit)
  console.log("ℹ️ nextToken", nextToken)

  if(!authUserID){
    console.log("💥 'x-auth-user' not found")
    res.send({ code: "TOKEN_NOT_FOUND", reason: "Document for token not found"})
    return 
  }

  const feedUserID = profileID ? authUserID : 0 

  console.log("️ℹ️ using feedUserID", feedUserID)

  let nextDocument = undefined;
  if(nextToken) {
    nextDocument = await firestore.collection("feed")
      .doc(`${nextToken}_${feedUserID}`)
      .get()

      if(!nextDocument || !nextDocument.exists){
        res.statusCode = 404
        res.send({ code: "TOKEN_NOT_FOUND", reason: "Document for token not found"})
        return 
      }
  }

  let querySnapshotQuery = firestore.collection("feed")
    .where("feedUserID", "==", feedUserID)
    .orderBy("timestamp", 'desc')
    .limit(limit);

  if(nextDocument){
    querySnapshotQuery = querySnapshotQuery.startAfter(nextDocument)
  }

  querySnapshot = await querySnapshotQuery.get();

  console.log(`ℹ️ Got ${querySnapshot.docs.length} documents`);

  const feedItems = querySnapshot.docs.map(doc => ({
      firebaseID : doc.data().uid,
      type : doc.data().feedType
    }));

  const nextNewToken = feedItems.length > 0 ? feedItems[feedItems.length-1].firebaseID : undefined;

  const feedResult = {
    items: feedItems,
    nextToken: nextNewToken
  }

  res.send(feedResult);
});


module.exports = router;
